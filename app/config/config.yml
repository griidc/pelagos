imports:
    - { resource: parameters.yml }
    - { resource: security.yml }
    - { resource: services.yml }

# Put parameters here that don't need to change on each machine where the app is deployed
# http://symfony.com/doc/current/best_practices/configuration.html#application-related-configuration
parameters:
    locale: en
    jms_serializer.camel_case_naming_strategy.class: JMS\Serializer\Naming\IdenticalPropertyNamingStrategy
    router.request_context.host: "%hostname%"
    router.request_context.scheme: https
    # This parameter is undocumented. By default rabbitmq-supervisor-bundle uses old_sound_rabbit_mq -> consumers
    # from this config file to populate this parameter. As of rabbitmq-supervisor-bundle 1.5, only the array keys
    # are used so the values can be null.
    #phobetor_rabbitmq_supervisor.consumers:
    #   filer: ~
    #   create_homedir: ~

framework:
    #esi:             ~
    #translator:      { fallbacks: ["%locale%"] }
    secret:          "%secret%"
    router:
        resource: "%kernel.root_dir%/config/routing.yml"
        strict_requirements: ~
    form:            ~
    validation:      { enable_annotations: true }
    #serializer:      { enable_annotations: true }
    templating:
        engines: ['twig']
        #assets_version: SomeVersionScheme
    default_locale:  "%locale%"
    trusted_hosts:   ~
    trusted_proxies: ~
    session:
        # handler_id set to null will use default session handler from php.ini
        handler_id:  ~
        save_path:   "%kernel.root_dir%/../var/sessions/%kernel.environment%"
        storage_id: session.storage.native
    fragments:       ~
    http_method_override: true
    assets: ~

# Twig Configuration
twig:
    debug:            "%kernel.debug%"
    strict_variables: "%kernel.debug%"
    paths:
        "%kernel.root_dir%/../src/Pelagos/Bundle/AppBundle/Resources/views/template": templates
        "%kernel.root_dir%/../src/Pelagos/Bundle/AppBundle/Resources/views/DIF/email": DIFEmail
        "%kernel.root_dir%/../src/Pelagos/Bundle/AppBundle/Resources/views/Email": Email
    globals:
        google_maps_api_key: "%google_maps_api_key%"
        pelagos_readonly_message: "%pelagos_readonly_message%"

# Doctrine Configuration
doctrine:
    dbal:
        driver:   pdo_pgsql
        host:     "%database_host%"
        port:     "%database_port%"
        dbname:   "%database_name%"
        user:     "%database_user%"
        password: "%database_password%"
        charset:  UTF8
        types:
            text_array: "Doctrine\\DBAL\\PostgresTypes\\TextArrayType"
            xml: "Pelagos\\DoctrineExtensions\\DBAL\\Types\\PelagosXmlType"
            geometry: "Pelagos\\DoctrineExtensions\\DBAL\\Types\\GeometryType"
        mapping_types:
            _text: text_array
            xml: xml
            geometry: geometry
    orm:
        metadata_cache_driver: apc
        query_cache_driver: apc
        result_cache_driver: apc
        mappings:
            Pelagos:
                type: annotation
                dir: '%kernel.root_dir%/../src/Pelagos/Entity'
                is_bundle: false
                prefix: Pelagos\Entity
                alias: Pelagos
        naming_strategy: doctrine.orm.naming_strategy.underscore
        dql:
            string_functions:
                ST_Intersects: Jsor\Doctrine\PostGIS\Functions\ST_Intersects
                ST_GeomFromText: Jsor\Doctrine\PostGIS\Functions\ST_GeomFromText
                ST_AsText: Jsor\Doctrine\PostGIS\Functions\ST_AsText
                ST_GeomFromGML: Jsor\Doctrine\PostGIS\Functions\ST_GeomFromGML

# Swiftmailer Configuration
swiftmailer:
    transport: "%mailer_transport%"
    host:      "%mailer_host%"
    username:  "%mailer_user%"
    password:  "%mailer_password%"
    antiflood:
        threshold: "%mailer_antiflood_threshold%"

fos_rest:
    view:
        view_response_listener: true
        exception_wrapper_handler: Pelagos\Bundle\AppBundle\Handler\ExceptionWrapperHandler
    routing_loader:
        default_format: json
        include_format: false
    serializer:
        serialize_null: true
    exception:
        enabled: true
        messages:
            'Symfony\Component\HttpKernel\Exception\BadRequestHttpException': true

nelmio_api_doc:
    sandbox:
        request_format:
            method: accept_header

assetic:
    debug:          '%kernel.debug%'
    use_controller: '%kernel.debug%'
    filters:
        cssrewrite: ~
    bundles:
        - PelagosAppBundle

simple_things_entity_audit:
    audited_entities:
        - Pelagos\Entity\DataRepository
        - Pelagos\Entity\FundingOrganization
        - Pelagos\Entity\FundingCycle
        - Pelagos\Entity\ResearchGroup
        - Pelagos\Entity\Person
        - Pelagos\Entity\PersonDataRepository
        - Pelagos\Entity\PersonFundingOrganization
        - Pelagos\Entity\PersonResearchGroup
        - Pelagos\Entity\Dataset
        - Pelagos\Entity\DIF
        - Pelagos\Entity\DatasetSubmission
        - Pelagos\Entity\DataCenter
        - Pelagos\Entity\DistributionPoint

fos_js_routing:
    routes_to_expose: [ .* ]

old_sound_rabbit_mq:
    connections:
        default:
            host:     "%rabbit_mq_host%"
            port:     "%rabbit_mq_port%"
            user:     "%rabbit_mq_user%"
            password: "%rabbit_mq_pass%"
            vhost:    "%pelagos_name%"
            lazy:     true
            connection_timeout: 3
            read_write_timeout: 3
            keepalive: false
            heartbeat: 0
    producers:
        dataset_submission:
            connection:       default
            exchange_options: { name: 'dataset_submission', type: topic }
        create_homedir:
            connection:       default
            exchange_options: { name: 'create_homedir', type: direct }
        dataset_file_hasher:
            connection:       default
            exchange_options: { name: 'dataset_file_hasher', type: direct }
        doi_issue:
            connection:       default
            exchange_options: { name: 'doi_issue', type: topic }
    consumers:
        filer:
            connection:       default
            exchange_options: { name: 'dataset_submission', type: topic }
            callback:         pelagos.rabbit.consumer.dataset_submission.filer
            qos_options:      { prefetch_count: 1, global: false }
            queue_options:
                name: 'filer'
                routing_keys:
                    - 'dataset.upload'
                    - 'dataset.SFTP'
                    - 'metadata.#'
        retriever:
            connection:       default
            exchange_options: { name: 'dataset_submission', type: topic }
            callback:         pelagos.rabbit.consumer.dataset_submission.retriever
            qos_options:      { prefetch_count: 1, global: false }
            queue_options:
                name: 'retriever'
                routing_keys:
                    - 'dataset.HTTP'
        create_homedir:
            connection:       default
            exchange_options: { name: 'create_homedir', type: direct }
            callback:         pelagos.rabbit.consumer.create_homedir
            queue_options:
                name: 'create_homedir'
        dataset_file_hasher:
            connection:       default
            exchange_options: { name: 'dataset_file_hasher', type: direct }
            callback:         pelagos.rabbit.consumer.dataset_file_hasher
            qos_options:      { prefetch_count: 1, global: false }
            queue_options:
                name: 'dataset_file_hasher'
        doi_issue:
            connection:       default
            exchange_options: { name: 'doi_issue', type: topic }
            callback:         pelagos.rabbit.consumer.doi_issue
            qos_options:      { prefetch_count: 1, global: false }
            queue_options:
                name: 'doi_issue'
                routing_keys:
                    - 'issue'
                    - 'publish'
                    - 'update'
                    - 'delete'

hwi_oauth:
    firewall_names: [main]
    resource_owners:
        google:
            type:           google
            client_id:      "%google_oauth_client_id%"
            client_secret:  "%google_oauth_client_secret%"
            scope:          "https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile"
    use_referer: true

rabbit_mq_supervisor:
    supervisor_instance_identifier: "%pelagos_name%"
    commands:
        rabbitmq_consumer: rabbitmq:consumer -w %%2$s

fos_elastica:
    clients:
        default: { host: localhost, port: 9200 }
    indexes:
        pelagos:
            client: default
            index_name: "%pelagos_name%"
            types:
                dataset:
                    mappings:
                        udi: ~
                        title: { boost: 3 }
                        abstract: ~
                        availabilityStatus: ~
                        identifiedStatus: ~
                        researchGroup:
                            type: 'nested'
                            properties:
                                id: ~
                                name: ~
                                fundingCycle:
                                    type: 'nested'
                                    properties:
                                        id: ~
                                        fundingOrganization:
                                            type: 'nested'
                                            properties:
                                                id: ~
                        datasetSubmission:
                            type: 'nested'
                            properties:
                                authors: ~
                                datasetFileTransferStatus: ~
                                datasetFileUri: ~
                                datasetFileSize: ~
                                restrictions: ~
                                placeKeywords: { boost: 3 }
                                themeKeywords: { boost: 3 }
                        doi:
                            type: 'nested'
                            properties:
                                doi: { boost: 3 }
                        publications:
                            type: 'nested'
                            properties:
                                doi: { boost: 3 }
                        geometry:
                            type: 'string'
                            property_path: false
                        simpleGeometry:
                            type: 'geo_shape'
                            property_path: false
                        year:
                            property_path: false
                        estimatedStartDate:
                            property_path: false
                        estimatedEndDate:
                            property_path: false
                        updatedDateTime:
                            type: 'date'
                            format: 'basic_date_time_no_millis'
                            property_path: false
                    persistence:
                        driver: orm
                        model: Pelagos\Entity\Dataset
                        provider: ~
                        listener:
                            logger: true
                        finder: ~

doctrine_migrations:
    dir_name: "%kernel.root_dir%/DoctrineMigrations"
    namespace: Pelagos\Migrations
    table_name: migration_versions
    name: Pelagos Migrations
