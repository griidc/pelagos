<?php

namespace Pelagos;

/**
 * Dummy integration test so PHPUnit is happy.
 */
class DummyTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test nothing.
     *
     * @return void
     */
    public function testNothing()
    {
    }
}
