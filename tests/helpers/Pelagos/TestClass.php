<?php

namespace Pelagos;

/**
 * A test class.
 *
 * @foo bar
 *
 * @return something
 */
class TestClass
{

    /**
     * A test property.
     *
     * @var string
     *
     * @something else
     *
     * @blabla ding ding ding
     */
    private $testProp;

    /**
     * A test method.
     *
     * @type sometype
     *
     * @return void
     */
    public function testFunction()
    {
    }
}
