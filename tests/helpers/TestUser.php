<?php

namespace Pelagos\Tests\Helpers;

/**
 * Class to emulate Drupal $user.
 */
class TestUser
{
    /**
     * Username.
     *
     * @var string $name
     */
    public $name = 'test';
}
