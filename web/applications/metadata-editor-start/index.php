<?php 
// @codingStandardsIgnoreFile

require_once __DIR__.'/../../../vendor/autoload.php';

$GLOBALS['pelagos']['title'] = 'ISO 19115-2 Metadata Editor';

require 'start.html';
?>

<table width="700px">
    <tr>
        <td>
            <fieldset>
                <a href="http://data.gulfresearchinitiative.org/docs/metadata-editor/GRIIDC_ISO_19115-2_Metadata_Editor_Version_13.08_Release_Notes.pdf">GRIIDC ISO 19115-2 Metadata Editor Version 13.08 Release Notes</a>
            </fieldset>
        </td>
    </tr>
</table>

<?php require 'help.html'; ?>
