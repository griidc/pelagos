<?php

namespace Pelagos\Exception;

/**
 * Class InvalidTokenException.
 *
 * An exception to be thrown when the token is expired or invalid.
 *
 * @package Exception
 */
class InvalidTokenException extends \Exception
{
}
