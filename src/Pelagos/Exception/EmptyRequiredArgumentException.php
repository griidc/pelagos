<?php

namespace Pelagos\Exception;

/**
 * Custom exception for errors related to empty required arguments.
 */
class EmptyRequiredArgumentException extends ArgumentException
{
}
