<?php

namespace Pelagos\Exception;

/**
 * Custom exception for when a record already exists in persistence.
 */
class RecordExistsPersistenceException extends PersistenceException
{
}
