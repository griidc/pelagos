<?php

namespace Pelagos\Exception;

/**
 * Class AccountExistsException.
 *
 * An exception to be thrown when an someone request and account
 * but the acocunt already exists.
 *
 * @package Exception
 */
class AccountExistsException extends \Exception
{
}
