<?php

namespace Pelagos\Exception;

/**
 * Class InvalidXmlException.
 *
 * An exception to be thrown when text
 * expected to be XML does not validate.
 *
 * @package Exception
 */
class InvalidXmlException extends \Exception
{
}
