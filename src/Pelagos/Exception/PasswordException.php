<?php

namespace Pelagos\Exception;

/**
 * Custom exception for errors related passwords.
 */
class PasswordException extends \Exception
{
}
