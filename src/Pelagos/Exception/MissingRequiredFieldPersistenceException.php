<?php

namespace Pelagos\Exception;

/**
 * Custom exception for when a required field is missing in persistence.
 */
class MissingRequiredFieldPersistenceException extends PersistenceException
{
}
