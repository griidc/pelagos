<?php

namespace Pelagos\Exception;

/**
 * Class InvalidGmlException.
 *
 * An exception to be thrown upon finding invalid GML.
 *
 * @package Exception
 */
class InvalidGmlException extends \Exception
{
}
