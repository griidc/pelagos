<?php

namespace Pelagos\Response;

use Symfony\Component\HttpFoundation\Response;

/**
 * Class for custom Terminate Response.
 */
class TerminateResponse extends Response
{
}
