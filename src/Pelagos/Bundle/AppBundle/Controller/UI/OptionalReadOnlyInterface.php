<?php

namespace Pelagos\Bundle\AppBundle\Controller\UI;

/**
 * This empty interface simply serves as a designation of class needing selective read-only enforcement.
 */
interface OptionalReadOnlyInterface
{
}
