<?php

namespace Pelagos\Bundle\LegacyBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * A bundle for running Pelagos legacy (pre-Symfony) components.
 */
class PelagosLegacyBundle extends Bundle
{
}
